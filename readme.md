constructor <-> match

application <-> pi/lambda bindings

```lean
def main : IO () := do
    line <- IO.getLine
    IO.putLine line
    pure ()
```
