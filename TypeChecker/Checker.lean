structure Context where
  «local»: Local
  defs: Definitions

abbrev ContextM (a: Type) := Context -> (a × Context)

inductive Judgement
| mk (a: Term) (b: Term)

inductive CheckerE : Type -> Type
| onError (msg: String) : CheckerE Unit
| onHole (msg: String) : CheckerE Unit
